let $template = document.querySelector("#template");
let $fragment = document.createDocumentFragment();
let $card = document.querySelector(".cards");

let $datos = [
  {
    cita: "juan 3.16",
    versiculo:
      "Porque de tal manera amó Dios al mundo, que ha dado a su Hijo unigénito, para que todo aquel que en él cree, no se pierda, mas tenga vida eterna.",
    color: "background: radial-gradient(#1fe4f5, #3fbafe)",
  },
  {
    cita: "1 Pedro 5:6",
    versiculo:
      "Humillaos, pues, bajo la poderosa mano de Dios, para que él os exalte cuando fuere tiempo",
    color: "background: radial-gradient(#fbc1cc, #fa99b2)",
  },
  {
    cita: "2 Timoteo 4:2",
    versiculo:
      "Que prediques la palabra; que instes a tiempo y fuera de tiempo; redarguye, reprende, exhorta con toda paciencia y doctrina.",
    color: "background: radial-gradient(#76b2fe, #b69efe)",
  },
  {
    cita: "Genesis 1:2",
    versiculo:
      "Y la tierra estaba desordenada y vacía, y las tinieblas estaban sobre la faz del abismo, y el Espíritu de Dios se movía sobre la faz de las aguas.",
    color: "background: radial-gradient(#60efbc, #58d5c9)",
  },
  {
    cita: "Apocalipsis 3:20",
    versiculo:
      "He aquí, yo estoy a la puerta y llamo; si alguno oye mi voz y abre la puerta, entraré a él, y cenaré con él, y él conmigo.",
    color: "background: radial-gradient(#f588d8, #c0a3e5)",
  },
  {
    cita: "Hechos 1:8",
    versiculo:
      "pero recibiréis poder, cuando haya venido sobre vosotros el Espíritu Santo, y me seréis testigos en Jerusalén, en toda Judea, en Samaria, y hasta lo último de la tierra.",
    color: "background: radial-gradient(#b7950b, #f7dc6f )",
  },
];

const mostrarCapitulos = () => {
  $datos.map((key) => {
    let { cita, versiculo, color } = key;

    let clone = $template.content.cloneNode(true);
    clone.querySelector(".card__title").textContent = versiculo;
    clone.querySelector(".card__link").textContent = cita;
    clone.querySelector(".card").setAttribute("style", color);

    $fragment.appendChild(clone);
  });
  $card.appendChild($fragment);
};

mostrarCapitulos();
